from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QWidget, QVBoxLayout

import BarchartPortfolios as BP


class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = 'Packaged Portfolios'
        self.setWindowTitle(self.title)

        self.table_widget = MyTableWidget(self)
        self.setCentralWidget(self.table_widget)


        #self.setMinimumSize(700,500)

        self.show()


class MyTableWidget(QWidget):

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        #self.layout = QVBoxLayout(self)


        self.results_box = QMessageBox()

        self.layout = QVBoxLayout(self)
        self.layout.setAlignment(Qt.AlignTop)

        self.titleLabel = QLabel("Choose any of our market-ready portfolios")
        self.titleLabel.setAlignment(Qt.AlignCenter)
        
        # ----------
        self.p1Label = QLabel("Best value stocks portfolio (all stocks)")
        self.p1Label.setAlignment(Qt.AlignLeft)

        self.group1 = QGroupBox(self)
        self.p1Layout = QHBoxLayout(self)

        self.a1Label = QLabel("Fund Allocation:")
        self.a1Input = QLineEdit("100000")
        self.s1Label = QLabel("Amount of stocks to invest in:")
        self.s1Input = QLineEdit("25")
        self.p1Button = QPushButton("View Portfolio")

        self.p1Button.clicked.connect(self.p1)

        self.p1Layout.addWidget(self.a1Label)
        self.p1Layout.addWidget(self.a1Input)
        self.p1Layout.addWidget(self.s1Label)
        self.p1Layout.addWidget(self.s1Input)
        self.p1Layout.addWidget(self.p1Button)

        self.group1.setLayout(self.p1Layout)

        # ---------------

        self.p2Label = QLabel("Best value stocks portfolio (micro-cap)")
        self.p2Label.setAlignment(Qt.AlignLeft)

        self.group2 = QGroupBox(self)
        self.p2Layout = QHBoxLayout(self)

        self.a2Label = QLabel("Fund Allocation:")
        self.a2Input = QLineEdit("100000")
        self.s2Label = QLabel("Amount of stocks to invest in:")
        self.s2Input = QLineEdit("25")
        self.p2Button = QPushButton("View Portfolio")

        self.p2Button.clicked.connect(self.p2)

        self.p2Layout.addWidget(self.a2Label)
        self.p2Layout.addWidget(self.a2Input)
        self.p2Layout.addWidget(self.s2Label)
        self.p2Layout.addWidget(self.s2Input)
        self.p2Layout.addWidget(self.p2Button)

        self.group2.setLayout(self.p2Layout)
        
        # ---------------

        self.p3Label = QLabel("Best price-to-sales and momentum composite portfolio (micro-cap)")
        self.p3Label.setAlignment(Qt.AlignLeft)

        self.group3 = QGroupBox(self)
        self.p3Layout = QHBoxLayout(self)

        self.a3Label = QLabel("Fund Allocation:")
        self.a3Input = QLineEdit("100000")
        self.s3Label = QLabel("Amount of stocks to invest in:")
        self.s3Input = QLineEdit("25")
        self.p3Button = QPushButton("View Portfolio")

        self.p3Button.clicked.connect(self.p3)

        self.p3Layout.addWidget(self.a3Label)
        self.p3Layout.addWidget(self.a3Input)
        self.p3Layout.addWidget(self.s3Label)
        self.p3Layout.addWidget(self.s3Input)
        self.p3Layout.addWidget(self.p3Button)

        self.group3.setLayout(self.p3Layout)
        
        # ---------
        
        self.p4Label = QLabel("Worst fundamental stocks shorting portfolio (all stocks)")
        self.p4Label.setAlignment(Qt.AlignLeft)

        self.group4 = QGroupBox(self)
        self.p4Layout = QHBoxLayout(self)

        self.a4Label = QLabel("Fund Allocation:")
        self.a4Input = QLineEdit("100000")
        self.s4Label = QLabel("Amount of stocks to invest in:")
        self.s4Input = QLineEdit("25")
        self.p4Button = QPushButton("View Portfolio")

        self.p4Button.clicked.connect(self.p4)

        self.p4Layout.addWidget(self.a4Label)
        self.p4Layout.addWidget(self.a4Input)
        self.p4Layout.addWidget(self.s4Label)
        self.p4Layout.addWidget(self.s4Input)
        self.p4Layout.addWidget(self.p4Button)

        self.group4.setLayout(self.p4Layout)

        # -------

        self.layout.addWidget(self.titleLabel)
        self.layout.addWidget(self.p1Label)
        self.layout.addWidget(self.group1)
        self.layout.addWidget(self.p2Label)
        self.layout.addWidget(self.group2)
        self.layout.addWidget(self.p3Label)
        self.layout.addWidget(self.group3)
        self.layout.addWidget(self.p4Label)
        self.layout.addWidget(self.group4)

        # The way to constantly update the output
        self.auto_update_all()
        self.update_timer = QTimer()
        self.update_timer.timeout.connect(self.auto_update_all)
        self.update_timer.start(1000)

        #---------------------------------------------------------------------------------------------------------------

    def auto_update_all(self):
        self.update()

    def p1(self):
        BP.ValueFactor1Portfolio(float(self.a1Input.text()), int(self.s1Input.text()))

    def p2(self):
        BP.MicroCapValuePortfilio(float(self.a2Input.text()), int(self.s2Input.text()))

    def p3(self):
        BP.MicroCapMomentumAndPSRPortfolio(float(self.a3Input.text()), int(self.s3Input.text()))

    def p4(self):
        BP.WorstFundamentalsShortingPortfolio(float(self.a4Input.text()), int(self.s4Input.text()))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

