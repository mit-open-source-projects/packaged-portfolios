# Packaged Portfolios
Fundamental stock screens inspired by the book What Works on Wall Street by James O'Shaunnessy

## Usage
Just run PackagedPortfoliosGUI.py 

## Note
* The csv files are downloaded from the stock screener at https://www.barchart.com/stocks/stocks-screener
* The csv files provided are not up to date and are left in this repository for this program to work as a demo

## Disclaimer 
The results from these are not recommendations for investment decisions but simply a ranking of financial ratio factors. 
