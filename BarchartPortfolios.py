import os
from BarchartScreener import *

# Note: These are annually rebalancing portfolios (technically could be semi-annually as well)

def MicroCapMomentumAndPSRPortfolio(allocation, portfolioSize):
    allocation_per_ticker = allocation / portfolioSize

    df = MicroCapMomentumPSR()
    df = df.head(portfolioSize)

    df['SharesToAllocate'] = np.floor(allocation_per_ticker / df['Last'])

    cols = list(df.columns.values)

    cols[5], cols[len(cols)-1] = cols[len(cols)-1], cols[5]

    df = df.reindex(columns=cols)

    df.to_csv('Portfolios/Micro Cap Momentum and PSR Portfolio.csv')

    if os.name == 'nt':  # If it is windows
        os.system('start Portfolios/Micro\ Cap\ Momentum\ and\ PSR\ Portfolio.csv')

    else:  # If it is mac or linux
        os.system('open Portfolios/Micro\ Cap\ Momentum\ and\ PSR\ Portfolio.csv')

def MicroCapValuePortfilio(allocation, portfolioSize):
    allocation_per_ticker = allocation / portfolioSize

    df = MicroCapValue()
    df = df.head(portfolioSize)

    df['SharesToAllocate'] = np.floor(allocation_per_ticker / df['Last'])

    cols = list(df.columns.values)

    cols[5], cols[len(cols)-1] = cols[len(cols)-1], cols[5]

    df = df.reindex(columns=cols)

    df.to_csv('Portfolios/Micro Cap Value Portfolio.csv')

    if os.name == 'nt': # If it is windows
        os.system('start Portfolios/Micro\ Cap\ Value\ Portfolio.csv')

    else: # If it is mac or linux
        os.system('open Portfolios/Micro\ Cap\ Value\ Portfolio.csv')

def ValueFactor1Portfolio(allocation, portfolioSize):
    allocation_per_ticker = allocation / portfolioSize

    df = valueFactor1()
    df = df.head(portfolioSize)

    df['SharesToAllocate'] = np.floor(allocation_per_ticker / df['Last'])

    cols = list(df.columns.values)

    cols[5], cols[len(cols)-1] = cols[len(cols)-1], cols[5]

    df = df.reindex(columns=cols)

    df.to_csv('Portfolios/All Stocks Best Fundamentals Portfolio.csv')

    if os.name == 'nt': # If it is windows
        os.system('start Portfolios/All\ Stocks\ Best\ Fundamentals\ Portfolio.csv')

    else: # If it is mac or linux
        os.system('open Portfolios/All\ Stocks\ Best\ Fundamentals\ Portfolio.csv')


def WorstFundamentalsShortingPortfolio(allocation, portfolioSize):
    allocation_per_ticker = allocation / portfolioSize

    df = worstFundamentals()
    df = df.head(portfolioSize)

    df['SharesToAllocate'] = np.floor(allocation_per_ticker / df['Last'])

    cols = list(df.columns.values)

    cols[5], cols[len(cols)-1] = cols[len(cols)-1], cols[5]

    df = df.reindex(columns=cols)

    df.to_csv('Portfolios/Worst Fundamentals Shorting Portfolio.csv')

    if os.name == 'nt': # If it is windows
        os.system('start Portfolios/Worst\ Fundamentals\ Shorting\ Portfolio.csv')

    else: # If it is mac or linux
        os.system('open Portfolios/Worst\ Fundamentals\ Shorting\ Portfolio.csv')

