import numpy as np
import pandas as pd
import math

csv_to_use = 'Data/BarchartValueTrendMain.csv'

'''
Best:
1. Price-to-Book
2. Price-to-Earnings
3. Price-to-Sales
4. EBITDA/EV
5. Price-to-Cash Flow
6. Shareholder Yield

Equally weighted

Decile 1 (top 10 %)
top 25 Stocks with best 6-Month price Appretiation 
'''
def trendingValue():
    df = pd.read_csv(csv_to_use)

    # PE Ratio > 0
    #df = df[df['P/E (ttm)'] > 0]

    # Making this column for the scoring since it is not provided
    # TODO: make it EBIT / EV and not market cap
    df['EBIT/MC'] = df['EBIT'] / df['Market Cap'] # Adding this row for the score

    # The scoring of the factors
    df['P/B Score'] = (1 - df['Price/Book'].rank(pct=True)) * 100
    df['P/E Score'] = (1 - df['P/E (ttm)'].rank(pct=True)) * 100
    df['P/S Score'] = (1 - df['P/S Ratio'].rank(pct=True)) * 100
    df['EBITDA/MC Score'] = df['EBIT/MC'].rank(pct=True) * 100
    df['Price/Cash Flow Score'] = (1 - df['P/CF Ratio'].rank(pct=True)) * 100

    # The total rankings
    df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['EBITDA/MC Score'] + df['Price/Cash Flow Score']
    #df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['Price/Cash Flow Score']

    df = df.sort_values(by='Overall Stock Score', ascending=False)

    df = df.reset_index()
    df.to_csv('Results/trending_value_micro_cap_results.csv')

    return df


'''
Value Factor 1 to get best 10% 
Then Chooses ones based off of their weighted alphas
'''
def microCapValueAndTrend():
    df = pd.read_csv(csv_to_use)

    # Market Cap conditions
    df = df[df['Market Cap'] < 250000000]
    df = df[df['Market Cap'] > 50000000]

    # PE Ratio > 0
    df = df[df['P/E (ttm)'] > 0]

    # Making this column for the scoring since it is not provided
    df['EBIT/MC'] = df['EBIT'] / df['Market Cap'] # Adding this row for the score

    # The scoring of the factors
    df['P/B Score'] = (1 - df['Price/Book'].rank(pct=True)) * 100
    df['P/E Score'] = (1 - df['P/E (ttm)'].rank(pct=True)) * 100
    df['P/S Score'] = (1 - df['P/S Ratio'].rank(pct=True)) * 100
    df['EBITDA/MC Score'] = df['EBIT/MC'].rank(pct=True) * 100
    df['Price/Cash Flow Score'] = (1 - df['P/CF Ratio'].rank(pct=True)) * 100
    #df['Alpha Score'] = df['Wtd Alpha'].rank(pct=True) * 100

    # The total rankings
    df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['EBITDA/MC Score'] + \
                                df['Price/Cash Flow Score']

    df = df.sort_values(by='Overall Stock Score', ascending=False)

    filterTop = np.floor(len(df.index) * .05)
    df = df.head(int(filterTop))

    df = df.sort_values(by='Wtd Alpha', ascending=True)
    df = df.reset_index()
    df.to_csv('Results/microCapValueAndTrendResults.csv')
    df = pd.read_csv('Results/microCapValueAndTrendResults.csv')
    df = df.sort_values(by='Wtd Alpha', ascending=False)
    df = df.reset_index()
    df.to_csv('Results/microCapValueAndTrendResults.csv')

    return df



# Same as valuefactor1_All_Stocks but excludes small market cap stocks
def valueFactor1():
    df = pd.read_csv(csv_to_use)

    df = df[df['P/E (ttm)'] > 0]

    df['EBIT/MC'] = df['EBIT'] / df['Market Cap'] # Adding this row for the score
    df['P/B Score'] = (1 - df['Price/Book'].rank(pct=True)) * 100
    df['P/E Score'] = (1 - df['P/E (ttm)'].rank(pct=True)) * 100
    df['P/S Score'] = (1 - df['P/S Ratio'].rank(pct=True)) * 100
    df['EBITDA/MC Score'] = df['EBIT/MC'].rank(pct=True) * 100
    df['Price/Cash Flow Score'] = (1 - df['P/CF Ratio'].rank(pct=True)) * 100
    df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['EBITDA/MC Score'] + df['Price/Cash Flow Score']
    #df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['Price/Cash Flow Score']

    df = df.sort_values(by='Overall Stock Score', ascending=False)


    df = df.reset_index()

    df.to_csv('Results/best_micro_value_results_bc.csv')

    return df

# Same as valuefactor1_All_Stocks but excludes small market cap stocks
def valueFactor1SP500():
    df = pd.read_csv('Data/sp500Test.csv')

    df = df[df['P/E (ttm)'] > 0]
    df['EBIT/MC'] = df['EBIT'] / df['Market Cap'] # Adding this row for the score
    df['P/B Score'] = (1 - df['Price/Book'].rank(pct=True)) * 100
    df['P/E Score'] = (1 - df['P/E (ttm)'].rank(pct=True)) * 100
    df['P/S Score'] = (1 - df['P/S Ratio'].rank(pct=True)) * 100
    df['EBITDA/MC Score'] = df['EBIT/MC'].rank(pct=True) * 100
    df['Price/Cash Flow Score'] = (1 - df['P/CF Ratio'].rank(pct=True)) * 100
    df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['EBITDA/MC Score'] + df['Price/Cash Flow Score']
    #df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['Price/Cash Flow Score']

    df = df.sort_values(by='Overall Stock Score', ascending=False)

    df = df.reset_index()

    df.to_csv('Results/sp500ValueResults.csv')

    return df

def valueFactor1Russell3000():
    df = pd.read_csv('Data/russell3000Test.csv')

    df = df[df['P/E (ttm)'] > 0]

    df['EBIT/MC'] = df['EBIT'] / df['Market Cap'] # Adding this row for the score
    df['P/B Score'] = (1 - df['Price/Book'].rank(pct=True)) * 100
    df['P/E Score'] = (1 - df['P/E (ttm)'].rank(pct=True)) * 100
    df['P/S Score'] = (1 - df['P/S Ratio'].rank(pct=True)) * 100
    df['EBITDA/MC Score'] = df['EBIT/MC'].rank(pct=True) * 100
    df['Price/Cash Flow Score'] = (1 - df['P/CF Ratio'].rank(pct=True)) * 100
    df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['EBITDA/MC Score'] + df['Price/Cash Flow Score']
    #df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['Price/Cash Flow Score']

    df = df.sort_values(by='Overall Stock Score', ascending=False)

    df = df.reset_index()

    df.to_csv('Results/russell3000ValueResults.csv')

    return df

def MicroCapValue():
    df = pd.read_csv(csv_to_use)

    # Market Cap conditions
    df = df[df['Market Cap'] < 250000000]
    df = df[df['Market Cap'] > 50000000]

    # PE Ratio > 0
    df = df[df['P/E (ttm)'] > 0]
    # Making this column for the scoring since it is not provided
    df['EBIT/MC'] = df['EBIT'] / df['Market Cap'] # Adding this row for the score
    # The scoring of the factors
    df['P/B Score'] = (1 - df['Price/Book'].rank(pct=True)) * 100
    df['P/E Score'] = (1 - df['P/E (ttm)'].rank(pct=True)) * 100
    df['P/S Score'] = (1 - df['P/S Ratio'].rank(pct=True)) * 100
    df['EBITDA/MC Score'] = df['EBIT/MC'].rank(pct=True) * 100
    df['Price/Cash Flow Score'] = (1 - df['P/CF Ratio'].rank(pct=True)) * 100

    # The total rankings
    df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['EBITDA/MC Score'] + \
                                df['Price/Cash Flow Score']

    df = df.sort_values(by='Overall Stock Score', ascending=False)
    df = df.reset_index()
    df.to_csv('Results/MicroCapValue.csv')

    return df


'''
Microcap 
PSR < 1.5
3 mon and 6 mon Mom. > 0 
Top 10 by 12 mon Mom
'''
def MicroCapMomentumPSR():
    df = pd.read_csv(csv_to_use)

    # Market Cap conditions
    df = df[df['Market Cap'] < 250000000]
    df = df[df['Market Cap'] > 50000000]

    df = df[df['P/S Ratio'] < 1]
    df = df[df['P/E (ttm)'] > 0]
    df = df[df['EBIT'] > 0]
    df = df[df['Price/Book'] > 0]

    # df = df.sort_values(by='Wtd Alpha', ascending=True)
    df = df.reset_index()
    df.to_csv('Results/MicroCapMomentumPSR.csv')
    df = pd.read_csv('Results/MicroCapMomentumPSR.csv')
    df = df.sort_values(by='Wtd Alpha', ascending=False)

    df = df.reset_index()
    df.to_csv('Results/MicroCapMomentumPSR.csv')

    return df


'''
Worst:
1. Price-to-Book
2. Price-to-Earnings
3. Price-to-Sales
4. EBITDA/EV
5. Price-to-Cash Flow

# -- 6. Shareholder Yield (dividend yield for now)

Equally Weighted (Or Heavily on P/S or EBITDA/EV)

Strategy:
Option 1: Short the worst 25 ones 
Option 2: Buy Long-Term put options on the worst 25 ones 
'''
def worstFundamentals():
    df = pd.read_csv(csv_to_use)

    # Making this column for the scoring since it is not provided
    # TODO: make it EBIT / EV and not market cap
    df['EBIT/MC'] = df['EBIT'] / df['Market Cap']  # Adding this row for the score

    # The scoring of the factors
    df['P/B Score'] = (1 - df['Price/Book'].rank(pct=True)) * 100
    df['P/E Score'] = (1 - df['P/E (ttm)'].rank(pct=True)) * 100
    df['P/S Score'] = (1 - df['P/S Ratio'].rank(pct=True)) * 100
    df['EBITDA/MC Score'] = df['EBIT/MC'].rank(pct=True) * 100
    df['Price/Cash Flow Score'] = (1 - df['P/CF Ratio'].rank(pct=True)) * 100

    # The total rankings
    df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['EBITDA/MC Score'] + df[
        'Price/Cash Flow Score']
    # df['Overall Stock Score'] = df['P/B Score'] + df['P/E Score'] + df['P/S Score'] + df['Price/Cash Flow Score']

    df = df.sort_values(by='Overall Stock Score', ascending=True)

    df = df.reset_index()
    df.to_csv('Results/vc2_inverse_results.csv')

    return df
